<?php
    require_once('./config/conexion.php');
    if(isset($_POST['enviar']) and $_POST['enviar'] == 'si'){
        require_once("models/User.php");
        $usuario = new User();
        $usuario->login();
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
    <div class="page-center">
        <div class="page-center-in">
            <div class="container container-fluid mt-4">
                <form class="sign-box" action="" method="POST" id="login_form">
                    <header class="sign-title">Acceso</header>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" name="usu_correo">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" name="usu_pass">
                    </div>
                    <div class="form-group">
                        <div class="float-right reset">
                            <a href="">Reset Password</a>
                        </div>
                    </div>
                    <input type="hidden" name="enviar" class="form-control" value="si">
                    <button type="submit" class="btn btn-rounded">Sign in</button>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</body>

</html>