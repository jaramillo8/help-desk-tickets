<?php

class User extends Conectar
{
  public function login() {
    $conectar = parent::conexion();
    parent::set_names();
    if(isset($_POST["enviar"])){
      $correo = $_POST['usu_correo'];
      $password = $_POST['usu_pass'];
      if(empty($correo) and empty($password)){
        header("Location:".conectar::ruta()."index.php?m=2");
        exit();
      }else{
        $sql = "SELECT * FROM tm_usuario WHERE usu_correo=? and usu_pass=? and estado=1";
        $stmt = $conectar->prepare($sql);
        $stmt->bindParam(1, $correo);
        $stmt->bindParam(2, $password);
        $stmt->execute();
        $resultado = $stmt->fetch();

        if(is_array($resultado) and count($resultado) > 0){
          $_SESSION["usu_id"]=$resultado["usu_id"];
          $_SESSION["usu_nom"]=$resultado["usu_nom"];
          $_SESSION["usu_ape"]=$resultado["usu_ape"];
          header("Location:".conectar::ruta()."views/home/");
          exit();
        }else{
          header("Location:".conectar::ruta()."index.php?m=1");
          exit();
        }
      }
    }
  }
}